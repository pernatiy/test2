<?php


namespace app\commands;


use app\components\SerferLogParser;
use yii\console\Controller;

/***
 *
 * Здесь реализована точка входа для обработки данных из начальной БД
 * Чтобы данный экшен можно было перезапускать - будем сохранять текущий-обрбонный примари из сторонней БД
 *
 */
class SerferController extends Controller
{

    public function actionParse()
    {
        $serferLogParser = new SerferLogParser();
        $serferLogParser->readLimit = 1000;
        while (1) {
            $serferLogParser->parse();
        }

    }
}
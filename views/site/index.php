<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\SerferBySecondSearch */
/* @var $ammount int */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?=$this->render('_search', ['model' => $searchModel]); ?>

    <?php if(!is_null($ammount)): ?>
        Кол-во серферов за указанный период: <?=$ammount?>
    <?php endif;?>


</div>

<?php

declare(strict_types=1);

namespace app\components;

use app\models\SerferBySecond;
use app\models\SerferSetting;
use yii\helpers\Json;

/**
 * Компонент который обработывает исходную БД и пишет данные в промежуточную с агрегацией кол-ва серферов по секундам
 */
class SerferLogParser
{
    /**
     * поле в DB serfer_setting для сохранения текущего primary
     */
    const SETTING_PRIMARY_FIELD = 'current_primary';

    /**
     * лимит одноразового чтения из началной DB
     */
    public $readLimit = 10000;

    /**
     * @var $currentPrimary int
     */
    protected $currentPrimary;

    /**
     * @var $data array Данный из начальной БД
     */
    protected $data;

    /**
     * @var $openedSerfers array открытые соединения, которые не закрылись в текщей выборке, будем хранить в редисе
     */
    protected $openedSerfers = [];

    /**
     * @var $redisOpenSerfersKey string клю для хранения текущих соединений в редисе
     */
    protected $redisOpenSerfersKey = 'openSerfersKey';

    protected function getCurrentPrimary()
    {
        $this->currentPrimary = SerferSetting::find()->where(['setting' => self::SETTING_PRIMARY_FIELD])->scalar();
    }

    protected function getOpenedSerfers()
    {
        $this->openedSerfers = Json::decode(\Yii::$app->redisCache->get($this->redisOpenSerfersKey));
    }


    protected function saveOpenedSerfers()
    {
        \Yii::$app->redis->set('openSerfers', Json::encode($this->redisOpenSerfersKey));
    }

    protected function setCurrentPrimary()
    {
        SerferSetting::updateAll(['value' => $this->currentPrimary], ['setting' => self::SETTING_PRIMARY_FIELD]);
    }

    protected function getData()
    {
        $this->data = \Yii::$app->db->createCommand(
            "SELECT * FROM base_table WHERE `primary` > :current_primary LIMIT :limit",
            [
                ':current_primary' => $this->currentPrimary,
                ':limit' => $this->readLimit,
            ]);
    }


    protected function getPrepaireData()
    {
        /*
         * К сожелению В ТЗ в начальной BD нет данных по каким критериям можно идентифицировать различные серферы
         * но суть данного метода заключается в формировании массива [секунда => кол-во серферов] из текущей выборки и
         * прибавлять кол-во открытых соединений из $openedSerfers. Предварительно записав/удалив сюда открытые/закрытые соединения
         * */
    }

    /**
     * Сохраняем данные через модель SerferBySecond
     */
    protected function saveData()
    {
        foreach ($this->data as $second => $ammount) {
            $serferBySecond = new SerferBySecond([
                'second' => $second,
                'ammount' => $ammount,
            ]);
            $serferBySecond->save();
        }
    }

    public function parse()
    {
        $this->getCurrentPrimary();

        $this->getOpenedSerfers();

        $this->getData();

        $this->getPrepaireData();

        $this->saveData();

        $this->setCurrentPrimary();
    }


}
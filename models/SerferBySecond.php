<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "serfer_by_second".
 *
 * @property int $second
 * @property int|null $ammount
 */
class SerferBySecond extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'serfer_by_second';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['second'], 'required'],
            [['second', 'ammount'], 'integer'],
            [['second'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'second' => 'Second',
            'ammount' => 'Ammount',
        ];
    }
}

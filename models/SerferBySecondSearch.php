<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SerferBySecond;

/**
 * SerferBySecondSearch represents the model behind the search form of `app\models\SerferBySecond`.
 */
class SerferBySecondSearch extends SerferBySecond
{
    /**
     * @var $from int Начальная дата в секундаx (unix)
     */
    public $from;

    /**
     * @var $to int Конечная дата в секунда (unix)
     */
    public $to;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'integer'],
            [['from', 'to'], 'required'],
            [['ammount'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => 'Начальное время',
            'to' => 'Конечное время',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return int
     */
    public function search($params): ?int
    {
        $this->load($params);

        if (!$this->validate()){
            return null;
        }

        $query = SerferBySecond::find()
            ->alias('sbs')
            ->select('SUM(sbs.ammount)')
            ->where(['>=', 'second', $this->from])
            ->andWhere(['<=', 'second', $this->to])
        ;

        return $query->scalar();
    }
}

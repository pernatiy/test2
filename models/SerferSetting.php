<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "serfer_setting".
 *
 * @property string $setting
 * @property string|null $value
 */
class SerferSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'serfer_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['setting'], 'required'],
            [['setting', 'value'], 'string', 'max' => 50],
            [['setting'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'setting' => 'Setting',
            'value' => 'Value',
        ];
    }
}
